﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4.DAL
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descr { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }

        public Product()
        {

        }

        public Product(string name, string descr, decimal price)
        {
            Name = name;
            Descr = descr;
            Price = price;
        }
    }
}
