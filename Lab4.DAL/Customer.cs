﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4.DAL
{
    public class Customer
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Mail { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }
}
