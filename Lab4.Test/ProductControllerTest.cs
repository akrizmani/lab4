using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab4.Controllers;
using Lab4.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace Lab4.Test
{
    public class ProductControllerTest
    {
        private DbContextOptions<Lab4DbContext> _dbContextOptions;

        public ProductControllerTest()
        {
                _dbContextOptions = new DbContextOptionsBuilder<Lab4DbContext>()
                                        .UseInMemoryDatabase(databaseName: "Testing_db")
                                        .Options;
        }

        [Fact]
        public async void PostTest()
        {
            using (var context = new Lab4DbContext(_dbContextOptions))
            {
                var productAPI = new ProductController(context);
                for (int i = 0; i < 5; i++)
                {
                    Product tmpProduct = new Product
                    {
                        Name = "Samsung galaxy s8",
                        Category = "Smartphones",
                        Descr = "Samsungov flagship",
                        Price = 5000,
                    };
                    var result = await productAPI.Create(tmpProduct);

                    var badRequest = result as BadRequestObjectResult;

                    Assert.Null(badRequest);
                }
            }
        }

        [Fact]
        public async void GetTest()
        {
            using (var context = new Lab4DbContext(_dbContextOptions))
            {
                var productAPI = new ProductController(context);
                for (int i = 0; i < 5; i++)
                {
                    Product tmpProduct = new Product
                    {
                        Name = "Samsung galaxy s8",
                        Category = "Smartphones",
                        Descr = "Samsungov flagship",
                        Price = 5000,
                    };
                    productAPI.Create(tmpProduct);
                }
            }

            using (var context = new Lab4DbContext(_dbContextOptions))
            {
                var productApi = new ProductController(context);
                var result = await productApi.GetProduct(3);
                var okResult = result as OkObjectResult;

                Assert.NotNull(okResult);
                Assert.Equal(200, okResult.StatusCode);

                Product product = okResult.Value as Product;
                Assert.NotNull(product);
                Assert.Equal("Smartphones", product.Category);
            }

        }
    }
}
