﻿var base = {
    baseUrl: "api/Product/",
    getProducts: function() {
        return this.baseUrl + 'GetProducts';
    },
    token: ''
};

var getProducts = function () {

    $.ajax({
        type: "GET",
        url: base.getProducts(),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-version': '1.0'
        },
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            $('#list tbody').empty();
            $.each(response, function (i, product) {
                json_data = '<tr>' +
                    '<td>' + product.id + '</td>' +
                    '<td>' + product.name + '</td>' +
                    '<td>' + product.descr + '</td>' +
                    '<td>' + product.price + '</td>' +
                    '</tr>';
                $(json_data).appendTo('#list tbody');
            });
        }
    });
};

var getProducts2 = function () {

    $.ajax({
        type: "GET",
        url: base.getProducts(),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-version': '1.1'
        },
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            $('#list2 tbody').empty();
            $.each(response, function (i, product) {
                json_data = '<tr>' +
                    '<td>' + product.id + '</td>' +
                    '<td>' + product.name + '</td>' +
                    '<td>' + product.descr + '</td>' +
                    '<td>' + product.price + '</td>' +
                    '</tr>';
                $(json_data).appendTo('#list2 tbody');
            });
        }
    });
};

var login = function () {
    var $form = $("#theLoginForm");
    var data = {
        'userName': $('#UserNameLogin').val(),
        'password': $('#PasswordLogin').val()
    };
    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: $form.attr('action'),
        data: JSON.stringify(data),
        dataType: 'json',
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            $("#login").addClass('hidden');
            $("#register").addClass('hidden');
            $("#createProduct").addClass('hidden');
            localStorage.setItem('token', response);
            base.token = response;
        }
    });
};

var register = function () {
    var $form = $("#theRegisterForm");
    var data = {
        'userName': $('#UserName').val(),
        'password': $('#Password').val(),
        'mail': $("#Mail").val(),
        'phone': $("#Phone").val()
    };
    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        url: $form.attr('action'),
        data: JSON.stringify(data),
        dataType: 'json',
        error: function (error) {
            console.log(error);
        },
        success: function (response) {
            $("#login").addClass('hidden');
            $("#register").addClass('hidden');
            $("#createProduct").addClass('hidden');
            localStorage.setItem('token', response);
            base.token = response;
        }
    });
};

var createProduct = function () {
    var $form = $("#theCreateForm");
    var data = {
        'name': $('#Name').val(),
        'descr': $('#Descr').val(),
        'price': $('#Price').val()
    };
    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + base.token
        },
        url: $form.attr('action'),
        data: JSON.stringify(data),
        dataType: 'json',
        error: function (error) {
            console.log(error);
            alert("Prvo se morate prijaviti");
        },
        success: function (response) {            
            $("#login").addClass('hidden');
            $("#register").addClass('hidden');
            $("#createProduct").addClass('hidden');
            getProducts();
        }
    });
};

var createProduct2 = function () {
    var $form = $("#theCreate2Form");
    var data = {
        'name': $('#Name2').val(),
        'descr': $('#Descr2').val(),
        'price': $('#Price2').val(),
        'category': $("#Category2").val()
    };
    console.log(data);
    $.ajax({
        type: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            "Authorization": 'Bearer ' + base.token,
            'api-version': '1.1'
        },
        url: $form.attr('action'),
        data: JSON.stringify(data),
        dataType: 'json',
        error: function (error) {
            console.log(error);
            alert(error.responseText);
        },
        success: function (response) {
            $("#login").addClass('hidden');
            $("#register").addClass('hidden');
            $("#createProduct").addClass('hidden');
            getProducts2();
        }
    });
};

$('.login-button').click(function (e) {
    e.preventDefault();
    login();
});

$('#login #Password').keypress(function (e) {
    if (e.keyCode === 13) {
        login();
    }
});

$('.create').click(function (e) {
    e.preventDefault();
    createProduct();
});

$('#Price').keypress(function (e) {
    if (e.keyCode === 13) {
        createProduct();
    }
});

$('.create2').click(function (e) {
    e.preventDefault();
    createProduct2();
});

$('#Category2').keypress(function(e) {
    if (e.keyCode === 13) {
        createProduct2();
    }
});

$('.register-button').click(function (e) {
    e.preventDefault();
    register();
});

$("#register #Password").keypress(function (e) {
    if (e.keyCode === 13) {
        register();
    }
});

$('.showLoginForm').click(function (e) {
    e.preventDefault();
    $("#login").removeClass('hidden');
    $("#register").addClass('hidden');
    $("#createProduct").addClass('hidden');
});

$('.showRegisterForm').click(function (e) {
    e.preventDefault();
    $("#login").addClass('hidden');
    $("#register").removeClass('hidden');
    $("#createProduct").addClass('hidden');
});

$('.showCreateForm').click(function (e) {
    e.preventDefault();
    $("#login").addClass('hidden');
    $("#register").addClass('hidden');
    $("#createProduct").removeClass('hidden');
});


$('.showCreate2Form').click(function (e) {
    e.preventDefault();
    $("#createProduct2").removeClass('hidden');
});

$(document).ready(function () {
    getProducts();
    getProducts2();
});

