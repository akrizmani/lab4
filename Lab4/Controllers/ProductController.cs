﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Lab4.DAL;
using Microsoft.EntityFrameworkCore;

namespace Lab4.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/Product")]
    [Authorize]
    public class ProductController : Controller
    {
        private readonly Lab4DbContext _context;

        public ProductController(Lab4DbContext context)
        {
            _context = context;
        }
        
        [AllowAnonymous]
        [HttpGet("GetProducts")]
        public IEnumerable<Product> GetProducts()
        {
            return _context.Product.Take(10);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute]int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                

            var product = await _context.Product.SingleOrDefaultAsync(x => x.Id == id);

            if (product == null)
            {
                return NotFound();
            }
                

            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            _context.Product.Add(product);
            await _context.SaveChangesAsync();

            return Ok("Product dodan");
        }
    }

    [ApiVersion("1.1")]
    [Produces("application/json")]
    [Route("api/Product")]
    [Authorize]
    public class ProductControllerV1_1 : Controller
    {
        private readonly Lab4DbContext _context;

        public ProductControllerV1_1(Lab4DbContext context)
        {
            _context = context;
        }

        [AllowAnonymous]
        [HttpGet("GetProducts")]
        public IEnumerable<Product> GetProductsV1_1()
        {
            return _context.Product.Take(5).OrderByDescending(x => x.Id);
        }

        [HttpPost("Create")]
        public IActionResult Create([FromBody] Product product)
        {
            if (string.IsNullOrEmpty(product.Category))
            {
                return BadRequest("Kategorija je obavezna");
            }
            _context.Product.Add(product);
            _context.SaveChanges();

            return Ok("Product dodan");
        }
    }



}