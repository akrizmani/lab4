﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Lab4.DAL;
using Lab4.Helpers;
using Lab4.Models;

namespace Lab4.Controllers
{
    [ApiVersion("1.0"), ApiVersion("1.1")]
    public class TokenController : Controller
    {
        private readonly Lab4DbContext _context;
        private readonly IConfiguration _configuration;

        public TokenController(Lab4DbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        [HttpPost("RequestToken")]
        public IActionResult RequestToken([FromBody] TokenRequestModel tokenRequest)
        {
            if (_context.Customer.Any(c => c.UserName == tokenRequest.UserName
                && c.Password == tokenRequest.Password))
            {
                JwtSecurityToken token = JwsTokenCreator.CreateToken(tokenRequest.UserName,
                _configuration["Auth:JwtSecurityKey"],
                _configuration["Auth:ValidIssuer"],
                _configuration["Auth:ValidAudience"]);
                string tokenStr = new JwtSecurityTokenHandler().WriteToken(token);

                return Ok(tokenStr);
            }
            return Unauthorized();
        }

        [HttpPost("CreateCustomer")]
        public IActionResult CreateCustomer([FromBody] TokenRequestModel tokenRequest)
        {
            var customer = new Customer
            {
                Mail = tokenRequest.Mail,
                Password = tokenRequest.Password,
                Phone = tokenRequest.Phone,
                UserName = tokenRequest.UserName
            };

            _context.Customer.Add(customer);
            if (_context.SaveChanges() > 0)
            {
                JwtSecurityToken token = JwsTokenCreator.CreateToken(tokenRequest.UserName,
                _configuration["Auth:JwtSecurityKey"],
                _configuration["Auth:ValidIssuer"],
                _configuration["Auth:ValidAudience"]);
                string tokenStr = new JwtSecurityTokenHandler().WriteToken(token);

                return Ok(tokenStr);
            }

            return BadRequest();
        }
    }
}